package com.comp502;

import java.util.ArrayList;
import java.util.List;

public class Inventory {
    List<Item> items;

    public Inventory() {
        this.items = new ArrayList<>(items);
    }

    public Inventory(List<Item> items) {
        this.items = items;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public void addItem(Item item) {
        this.items.add(item);
    }

    public void removeItem(Item item) {
        this.items.remove(item);
    }
}
