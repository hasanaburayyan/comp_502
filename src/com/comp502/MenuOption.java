package com.comp502;

public interface MenuOption {
    String getMenuText();
    int getOptionNumber();
    void setOptionNumber(int optionNumber);
}
