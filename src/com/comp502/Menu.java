package com.comp502;

import java.util.List;

public interface Menu {
    void addMenuOption(MenuOption option);

    void addMenuOptions(MenuOption ...options);

    String getMenuOutput();

    MenuOption getOptionChoiceFromInt(int choice);

    void determineNextActionFromOptionSelection(MenuOption optionSelected);
}
