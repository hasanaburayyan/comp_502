package com.comp502;

import java.util.ArrayList;
import java.util.List;

public class Store {
    List<Inventory> inventories;

    public Store() {
        this.inventories = new ArrayList<>();
    }

    public Store(List<Inventory> inventories) {
        this.inventories = inventories;
    }

    public List<Inventory> getInventories() {
        return inventories;
    }

    public void setInventories(List<Inventory> inventories) {
        this.inventories = inventories;
    }
}
