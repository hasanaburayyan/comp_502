package com.comp502;

import java.util.ArrayList;

import static com.comp502.MainMenu.Option.*;

public class MainMenu implements Menu {
    ArrayList<MenuOption> menuOptions;
    int optionCounter;

    public enum Option implements MenuOption {
        LIST_INVENTORIES("List All Current Inventories"),
        CREATE_NEW_INVENTORY("Create New Inventory"),
        EDIT_INVENTORY("Edit Existing Inventory"),
        DELETE_INVENTORY("Delete Existing Inventory"),
        EXIT_MENU("Exit Menu");

        String menuText;
        int optionNumber;

        @Override
        public String getMenuText() {
            return this.menuText;
        }
        @Override
        public void setOptionNumber(int optionNumber) {
            this.optionNumber = optionNumber;
        }
        @Override
        public int getOptionNumber() {
            return this.optionNumber;
        }

        Option(String menuText) {
            this.menuText = menuText;
        }

    }

    public MainMenu() {
        this.menuOptions = new ArrayList<>();
        this.optionCounter = 1;
    }

    @Override
    public void addMenuOption(MenuOption option) {
        this.menuOptions.add(option);
    }

    @Override
    public void addMenuOptions(MenuOption ...options) {
        for (int i = 0; i < options.length; i++) {
            options[i].setOptionNumber(this.optionCounter++);
            this.addMenuOption(options[i]);
        }
    }

    @Override
    public String getMenuOutput() {
        String menu = """
                Welcome To This Super Cool Inventory System!!
                
                please choose from the options below:
                
                """;
        for (MenuOption option : this.menuOptions) {
            menu += option.getOptionNumber() + ") " + option.getMenuText() + "\n";
        }
        return menu;
    }

    @Override
    public MenuOption getOptionChoiceFromInt(int choice) {
        for(MenuOption option : this.menuOptions) {
            if (option.getOptionNumber() == choice) {
                return option;
            }
        }
        return null;
    }

    @Override
    public void determineNextActionFromOptionSelection(MenuOption optionSelected) {
        if (LIST_INVENTORIES.menuText.equals(optionSelected.getMenuText())) {
            // TODO: Create Menu For Listing Inventories
            System.out.println("Need To List Inventories");
        } else if (CREATE_NEW_INVENTORY.menuText.equals(optionSelected.getMenuText())) {
            // TODO: Create Menu For Creating Inventories
            System.out.println("Need To Create Inventories");
        } else if (EDIT_INVENTORY.menuText.equals(optionSelected.getMenuText())) {
            // TODO: Create Menu For Editing Inventories
            System.out.println("Need To Edit Inventory");
        } else if (DELETE_INVENTORY.menuText.equals(optionSelected.getMenuText())) {
            // TODO: Create Menu For Deleting Inventories
            System.out.println("Need To Delete Inventory");
        } else if (EXIT_MENU.menuText.equals(optionSelected.getMenuText())) {
            System.out.println("Come Back Soon!...");
            System.exit(0);
        } else {
            System.out.println("Option Not Recognized");
        }
    }
}
