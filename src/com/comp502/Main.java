package com.comp502;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Store store = new Store();
        Scanner myScanner = new Scanner(System.in);

        while(true){
            // Get Menu Ready For User Input
            Menu mainMenu = createMainMenu();
            // Print Main Menu and get user input
            System.out.println(mainMenu.getMenuOutput());
            String userInput = myScanner.nextLine();

            // Get Menu Option From Choice
            MenuOption optionSelected = mainMenu.getOptionChoiceFromInt(Integer.parseInt(userInput));

            mainMenu.determineNextActionFromOptionSelection(optionSelected);
        }

    }

    public static Menu createMainMenu() {
        MainMenu menu = new MainMenu();
        MenuOption[] options = {
                MainMenu.Option.LIST_INVENTORIES,
                MainMenu.Option.CREATE_NEW_INVENTORY,
                MainMenu.Option.EDIT_INVENTORY,
                MainMenu.Option.DELETE_INVENTORY,
                MainMenu.Option.EXIT_MENU
        };

        menu.addMenuOptions(options);
        return menu;
    }
}
